
function BoutonDevis(props) {
	function clic() {
		if(props.clic!==undefined)
			props.clic()
	}
	return <button onClick={clic}>devis</button>	
}

class PetitesLignes extends React.Component {
	constructor(props) {
		super(props)
		this.state = { affiche:true }
	}
	masquer() {
		this.setState({ affiche:false })
	}
	render() {
		const style = {visibility: this.state.affiche ? 
			"visible" : "hidden" }
		return <p style={style}>Attention, le retour n'est pas 
			inclu dans le tarif.</p>
	}
}

class ZoneTexte extends React.Component {
	constructor(props) {
		super(props)
		this.sourisDedans = this.sourisDedans.bind(this)
		this.sourisDehors = this.sourisDehors.bind(this)
		this.changer = this.changer.bind(this)
		this.state = { "dedans": false, value:"" }
	}
	sourisDedans() {
		this.setState({ "dedans": true })
	}
	sourisDehors() {
		this.setState({ "dedans": false })
	}
	changer(e) {
		this.setState({ value:e.target.value })
	}
	lire() {
		return this.state.value
	}
	render() {
		let style = { background:"white" }
		if(this.state.dedans)
			style = { background:"#FDF" }
		return <p onMouseEnter={this.sourisDedans} 
				onMouseLeave={this.sourisDehors}>
			<label>{this.props.nom}</label>
			<input type="text" style={style} 
				value={this.state.value}
				onChange={this.changer} />
		</p>
	}
}

class Formulaire extends React.Component {
	constructor(props) {
		super(props)
		this.champs = {}
	}
	lireChamp(s) {
		return this.champs[s].lire()	
	}
	render() {
			return <form>
				<ZoneTexte nom="Prenom" 
					ref={o=>{this.champs.prenom=o} } />
				<ZoneTexte nom="Nom" 
					ref={o=>{this.champs.nom=o} } />
				<ZoneTexte nom="Telephone" 
					ref={o=>{this.champs['tel']=o} } />
				<ZoneTexte nom="Email" 
					ref={o=>{this.champs.email=o} } />
			</form>		
	}	
}

class GrandVoyage extends React.Component {
	constructor(props) {
		super(props)
		this.state = { message:"" }
		this.clic = this.clic.bind(this)
		//this.petitesLignes = null
		this.petitesLignes = React.createRef()
		this.formulaire = React.createRef()
	}
	clic() {
		const prenom = this.formulaire.current.lireChamp("prenom")
		const nom = this.formulaire.current.lireChamp("nom")
		console.log("Demande de devis")
		this.setState({ message : 
			<p>Demande de {prenom} {nom} envoyée</p>})
		// this.petitesLignes.masquer()
		this.petitesLignes.current.masquer()
	}
	render() {
		return <div>
			<h2>Notre grand voyage</h2>
			<p>Parcourez le désert et la savane..</p>
			<Formulaire ref={this.formulaire} />
			<BoutonDevis clic={this.clic} />
			{this.state.message}
			{/*<PetitesLignes ref={ o=>{this.petitesLignes=o} } />*/}
			<PetitesLignes ref={this.petitesLignes} />
		</div>
	}
}

function App() {
	return <section>
		<h1>Afrique</h1>
		<GrandVoyage />
	</section>
	
}

let root = document.getElementById('root')
ReactDOM.render(<App />, root)