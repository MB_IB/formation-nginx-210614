/**
* exemple : <Titre zone="Amérique latine" exclamation={true} />
* => <h1>Voyages en Amérique latine !</h1>
* filtrer plus en détail avec la bibliothèque tierce prop-types
*/
function Titre(props) {
	let excl = ""
	//if(props.exclamation!=undefined && props.exclamation=="true")
	if(props.exclamation)
		excl = " !"
	return <h1>Voyages en {props.zone}{excl}</h1>
}

function MarkettingHiver() {
	return <p>Retrouvez le soleil !</p>
}

function MarkettingEte() {
	return <p>Fuyez la chaleur !</p>
}

function Presentation() {
	let marketting = <MarkettingHiver />
	const mois = new Date().getMonth()
	if(mois>3 && mois<9)
		marketting = <MarkettingEte />
	return <div><p>
		Retrouvez nos voyages au Bresil, Panama, Uruguay
		</p>{marketting}</div>
}

/**
* <PromoDuMois nom="Escale en Argentine" depart={new Date(2021,9,1)} />
*/
class PromoDuMois extends React.Component {
	constructor(props) {
		super(props)
		this.state = { 
			prix : 1200 + parseInt(Math.random()*100),
			maintenant : new Date()
		}
	}
	
	componentDidMount() { 
		//this.state.nom += "!" // pas utilisé
		// this.setState({
		// 	nom : this.state.nom+"!"
		// })
		this.setState(function(vieuxState){
			return { nom : vieuxState.nom+"!" }
		})
		this.chrono = setInterval( ()=>this.uneSeconde(), 1000)
	}
	componentWillUnmount() { 
		clearInterval(this.chrono)
	}
	componentDidUpdate() { 
		// interdit - rec inf : this.setState(...)
	}
	uneSeconde() {
		this.setState( { maintenant : new Date() } ) 
	}
	
	render() {
		// interdit - rec inf : this.setState(...)
		const depart = this.props.depart
		const intervale = (depart.getTime()-this.state.maintenant.getTime())/1000
		const j = parseInt(intervale / (24*3600))
		const h = parseInt((intervale / 3600) % 24)
		const m = parseInt((intervale / 60) % 60)
		const s = parseInt(intervale % 60)
		return <div>
			<h2 {...this.props}>Voyage du mois</h2>		
			<p>{this.props.nom} : {this.state.prix} €</p>
			<p>Depart dans : {j}j{h}h{m}'{s}''</p>
		</div>
	}
}

function App() {
	return <div>
		<Titre zone="Amérique latine" exclamation={true} />
		<Presentation />
		<PromoDuMois nom="Balade à Cuidad Juarez" 
			depart={new Date(2021,7,4)} />
		<PromoDuMois nom="Escale en Argentine" 
			depart={new Date(2021,9,1)} />
	</div>	
}

let root = document.getElementById('root')
//ReactDOM.render(App(), root)
ReactDOM.render(<App />, root)