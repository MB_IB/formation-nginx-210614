
const ContexteOptions = React.createContext({})

class Option extends React.Component {
	constructor (props) {
		super(props)
		this.state = {"actif":false}
		this.check = this.check.bind(this)
	}
	check(e) {
		this.setState({ "actif": e.target.checked	})
		this.context.fonction(this.props.cle, e.target.checked)
	}
	componentDidMount() {
		this.setState({"actif":this.context.valeurs[this.props.cle]})
	}
	render() {
		return <p>
			<label>{this.props.nom}</label>
			<input type="checkbox" checked={this.state.actif} 
				onChange={this.check} />
		</p>
	}
}
Option.contextType = ContexteOptions

class Prix extends React.Component {
	render() {
		let prix = 1459
		if(this.context.valeurs.cite) prix += 58
		if(this.context.valeurs.chambre_individuelle) prix += 115
		if(this.context.valeurs.retour) prix += 640
		if(this.context.valeurs.visites) prix += 72
		return <p>Votre prix exclusif : {prix} €</p>
	}
}
Prix.contextType = ContexteOptions

class ChoixVoyage extends React.Component {
	constructor(props) {
		 super(props)
		 this.state = {
			cite:true, chambre_individuelle:true, retour:false, visites:true	
		}
		this.changeOption = this.changeOption.bind(this)
	}
	changeOption(cle, valeur) {
		const changement = {}
		changement[cle] = valeur
		this.setState(changement)
	}
	render() {
		return <article>
			<h2>Choisissez vos options</h2>
			<ContexteOptions.Provider value={{
				"valeurs":this.state,
				"fonction":this.changeOption
			}}>
				<Option nom="Cité interdite" cle="cite" />
				<Option nom="Chambre individuelle" cle="chambre_individuelle" />
				<Option nom="Retour inclus" cle="retour" />
				<Option nom="Visites guidées" cle="visites" />	
				<Prix />
			</ContexteOptions.Provider>	
		</article>	
	}
}

function App() {
	return <section>
		<h1>Asie</h1>
		<ChoixVoyage />
	</section>
	
}

let root = document.getElementById('root')
ReactDOM.render(<App />, root)