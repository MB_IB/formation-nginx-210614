class Section extends React.Component {
	
	render() {
		console.log(this.props.children)
		return <section>
			<h2>{this.props.titre}</h2>
			{this.props.children}
		</section>
	}
}

class Voyage extends React.Component {
	constructor(props) {
		super(props)
		this.clickCommander = this.clickCommander.bind(this)
	}
	nuitHotel() {
		return this.props.duree-1		
	}
	clickCommander() {
		console.log("Commande de "+this.props.nom)		
	}
	render() {
		let style = {}
		if(this.props.couleurTitre!==undefined)
			style.color = this.props.couleurTitre
		if(this.props.taillePoliceTitre!==undefined)
			style.fontSize = this.props.taillePoliceTitre 
		return <article className="voyage">
			<h3 style={style}>{this.props.nom} <span>({this.props.pays})</span></h3>
			<p>{this.props.duree} jours ({this.nuitHotel()} nuits)</p>
			<p>Seulement {this.props.prix} € !</p>
			{this.props.children}
			<button onClick={
				/* ()=>this.clickCommander() */
				/* (that)=>this.clickCommander(that) */
				this.clickCommander
			}>Commander</button>
		</article>
	}
}

function App() {
	return <div>
		<h1>Voyages en Europe</h1>
		<Section titre="Informations">
			<p>Des voyages de l'Irlande à la Turquie</p>
			<p>De l'Atlantique à La Caspienne, de la mer du Nord à la
				Mediterranée</p>
		</Section>
		<Section titre="Nos voyages">
			<Voyage nom="La Catalogne" prix={459} duree={5}
				pays="Espagne" couleurTitre="#B43" taillePoliceTitre="18pt">
				<p>Un voyage de Barcelone à Burgos</p>
			</Voyage>
			<Voyage nom="La Toscane" prix={699} duree={7}
				pays="Italie">
				<p>En car de Sienne à Bologne</p>
			</Voyage>
		</Section>
	</div>	
}

let root = document.getElementById('root')
//ReactDOM.render(App(), root)
ReactDOM.render(<App />, root)