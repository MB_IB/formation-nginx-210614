"use strict";

let root = document.getElementById('root');
let tels = ["0529291232", "0629323293", "0928372873"];
let email = "\"bob@voyages.fr\" => \"bob@voyages.com\"";
let infos = /*#__PURE__*/React.createElement("p", null, "Notre agence :", /*#__PURE__*/React.createElement("ul", null, /*#__PURE__*/React.createElement("li", null, "Pas d'adresse"), tels.map(function (t) {
  return /*#__PURE__*/React.createElement("li", null, "Tel : ", t);
}), tels.map(t => /*#__PURE__*/React.createElement("li", null, "Tel : ", t))));
ReactDOM.render( /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h1", null, "A propos"), infos, /*#__PURE__*/React.createElement("div", null, "Notre ", "e" + "mail", " : ", email)), root);