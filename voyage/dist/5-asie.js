"use strict";

const ContexteOptions = React.createContext({});

class Option extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      "actif": false
    };
    this.check = this.check.bind(this);
  }

  check(e) {
    this.setState({
      "actif": e.target.checked
    });
    this.context.fonction(this.props.cle, e.target.checked);
  }

  componentDidMount() {
    this.setState({
      "actif": this.context.valeurs[this.props.cle]
    });
  }

  render() {
    return /*#__PURE__*/React.createElement("p", null, /*#__PURE__*/React.createElement("label", null, this.props.nom), /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      checked: this.state.actif,
      onChange: this.check
    }));
  }

}

Option.contextType = ContexteOptions;

class Prix extends React.Component {
  render() {
    let prix = 1459;
    if (this.context.valeurs.cite) prix += 58;
    if (this.context.valeurs.chambre_individuelle) prix += 115;
    if (this.context.valeurs.retour) prix += 640;
    if (this.context.valeurs.visites) prix += 72;
    return /*#__PURE__*/React.createElement("p", null, "Votre prix exclusif : ", prix, " \u20AC");
  }

}

Prix.contextType = ContexteOptions;

class ChoixVoyage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cite: true,
      chambre_individuelle: true,
      retour: false,
      visites: true
    };
    this.changeOption = this.changeOption.bind(this);
  }

  changeOption(cle, valeur) {
    const changement = {};
    changement[cle] = valeur;
    this.setState(changement);
  }

  render() {
    return /*#__PURE__*/React.createElement("article", null, /*#__PURE__*/React.createElement("h2", null, "Choisissez vos options"), /*#__PURE__*/React.createElement(ContexteOptions.Provider, {
      value: {
        "valeurs": this.state,
        "fonction": this.changeOption
      }
    }, /*#__PURE__*/React.createElement(Option, {
      nom: "Cit\xE9 interdite",
      cle: "cite"
    }), /*#__PURE__*/React.createElement(Option, {
      nom: "Chambre individuelle",
      cle: "chambre_individuelle"
    }), /*#__PURE__*/React.createElement(Option, {
      nom: "Retour inclus",
      cle: "retour"
    }), /*#__PURE__*/React.createElement(Option, {
      nom: "Visites guid\xE9es",
      cle: "visites"
    }), /*#__PURE__*/React.createElement(Prix, null)));
  }

}

function App() {
  return /*#__PURE__*/React.createElement("section", null, /*#__PURE__*/React.createElement("h1", null, "Asie"), /*#__PURE__*/React.createElement(ChoixVoyage, null));
}

let root = document.getElementById('root');
ReactDOM.render( /*#__PURE__*/React.createElement(App, null), root);