"use strict";

/**
* exemple : <Titre zone="Amérique latine" exclamation={true} />
* => <h1>Voyages en Amérique latine !</h1>
* filtrer plus en détail avec la bibliothèque tierce prop-types
*/
function Titre(props) {
  let excl = ""; //if(props.exclamation!=undefined && props.exclamation=="true")

  if (props.exclamation) excl = " !";
  return /*#__PURE__*/React.createElement("h1", null, "Voyages en ", props.zone, excl);
}

function MarkettingHiver() {
  return /*#__PURE__*/React.createElement("p", null, "Retrouvez le soleil !");
}

function MarkettingEte() {
  return /*#__PURE__*/React.createElement("p", null, "Fuyez la chaleur !");
}

function Presentation() {
  let marketting = /*#__PURE__*/React.createElement(MarkettingHiver, null);
  const mois = new Date().getMonth();
  if (mois > 3 && mois < 9) marketting = /*#__PURE__*/React.createElement(MarkettingEte, null);
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("p", null, "Retrouvez nos voyages au Bresil, Panama, Uruguay"), marketting);
}
/**
* <PromoDuMois nom="Escale en Argentine" depart={new Date(2021,9,1)} />
*/


class PromoDuMois extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prix: 1200 + parseInt(Math.random() * 100),
      maintenant: new Date()
    };
  }

  componentDidMount() {
    //this.state.nom += "!" // pas utilisé
    // this.setState({
    // 	nom : this.state.nom+"!"
    // })
    this.setState(function (vieuxState) {
      return {
        nom: vieuxState.nom + "!"
      };
    });
    this.chrono = setInterval(() => this.uneSeconde(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.chrono);
  }

  componentDidUpdate() {// interdit - rec inf : this.setState(...)
  }

  uneSeconde() {
    this.setState({
      maintenant: new Date()
    });
  }

  render() {
    // interdit - rec inf : this.setState(...)
    const depart = this.props.depart;
    const intervale = (depart.getTime() - this.state.maintenant.getTime()) / 1000;
    const j = parseInt(intervale / (24 * 3600));
    const h = parseInt(intervale / 3600 % 24);
    const m = parseInt(intervale / 60 % 60);
    const s = parseInt(intervale % 60);
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h2", this.props, "Voyage du mois"), /*#__PURE__*/React.createElement("p", null, this.props.nom, " : ", this.state.prix, " \u20AC"), /*#__PURE__*/React.createElement("p", null, "Depart dans : ", j, "j", h, "h", m, "'", s, "''"));
  }

}

function App() {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Titre, {
    zone: "Am\xE9rique latine",
    exclamation: true
  }), /*#__PURE__*/React.createElement(Presentation, null), /*#__PURE__*/React.createElement(PromoDuMois, {
    nom: "Balade \xE0 Cuidad Juarez",
    depart: new Date(2021, 7, 4)
  }), /*#__PURE__*/React.createElement(PromoDuMois, {
    nom: "Escale en Argentine",
    depart: new Date(2021, 9, 1)
  }));
}

let root = document.getElementById('root'); //ReactDOM.render(App(), root)

ReactDOM.render( /*#__PURE__*/React.createElement(App, null), root);