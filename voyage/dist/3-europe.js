"use strict";

class Section extends React.Component {
  render() {
    console.log(this.props.children);
    return /*#__PURE__*/React.createElement("section", null, /*#__PURE__*/React.createElement("h2", null, this.props.titre), this.props.children);
  }

}

class Voyage extends React.Component {
  constructor(props) {
    super(props);
    this.clickCommander = this.clickCommander.bind(this);
  }

  nuitHotel() {
    return this.props.duree - 1;
  }

  clickCommander() {
    console.log("Commande de " + this.props.nom);
  }

  render() {
    let style = {};
    if (this.props.couleurTitre !== undefined) style.color = this.props.couleurTitre;
    if (this.props.taillePoliceTitre !== undefined) style.fontSize = this.props.taillePoliceTitre;
    return /*#__PURE__*/React.createElement("article", {
      className: "voyage"
    }, /*#__PURE__*/React.createElement("h3", {
      style: style
    }, this.props.nom, " ", /*#__PURE__*/React.createElement("span", null, "(", this.props.pays, ")")), /*#__PURE__*/React.createElement("p", null, this.props.duree, " jours (", this.nuitHotel(), " nuits)"), /*#__PURE__*/React.createElement("p", null, "Seulement ", this.props.prix, " \u20AC !"), this.props.children, /*#__PURE__*/React.createElement("button", {
      onClick:
      /* ()=>this.clickCommander() */

      /* (that)=>this.clickCommander(that) */
      this.clickCommander
    }, "Commander"));
  }

}

function App() {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h1", null, "Voyages en Europe"), /*#__PURE__*/React.createElement(Section, {
    titre: "Informations"
  }, /*#__PURE__*/React.createElement("p", null, "Des voyages de l'Irlande \xE0 la Turquie"), /*#__PURE__*/React.createElement("p", null, "De l'Atlantique \xE0 La Caspienne, de la mer du Nord \xE0 la Mediterran\xE9e")), /*#__PURE__*/React.createElement(Section, {
    titre: "Nos voyages"
  }, /*#__PURE__*/React.createElement(Voyage, {
    nom: "La Catalogne",
    prix: 459,
    duree: 5,
    pays: "Espagne",
    couleurTitre: "#B43",
    taillePoliceTitre: "18pt"
  }, /*#__PURE__*/React.createElement("p", null, "Un voyage de Barcelone \xE0 Burgos")), /*#__PURE__*/React.createElement(Voyage, {
    nom: "La Toscane",
    prix: 699,
    duree: 7,
    pays: "Italie"
  }, /*#__PURE__*/React.createElement("p", null, "En car de Sienne \xE0 Bologne"))));
}

let root = document.getElementById('root'); //ReactDOM.render(App(), root)

ReactDOM.render( /*#__PURE__*/React.createElement(App, null), root);