"use strict";

function BoutonDevis(props) {
  function clic() {
    if (props.clic !== undefined) props.clic();
  }

  return /*#__PURE__*/React.createElement("button", {
    onClick: clic
  }, "devis");
}

class PetitesLignes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      affiche: true
    };
  }

  masquer() {
    this.setState({
      affiche: false
    });
  }

  render() {
    const style = {
      visibility: this.state.affiche ? "visible" : "hidden"
    };
    return /*#__PURE__*/React.createElement("p", {
      style: style
    }, "Attention, le retour n'est pas inclu dans le tarif.");
  }

}

class GrandVoyage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ""
    };
    this.clic = this.clic.bind(this); //this.petitesLignes = null

    this.petitesLignes = React.createRef();
  }

  clic() {
    console.log("Demande de devis");
    this.setState({
      message: /*#__PURE__*/React.createElement("p", null, "Demande envoy\xE9e")
    }); // this.petitesLignes.masquer()

    this.petitesLignes.current.masquer();
  }

  render() {
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h2", null, "Notre grand voyage"), /*#__PURE__*/React.createElement("p", null, "Parcourez le d\xE9sert et la savane.."), /*#__PURE__*/React.createElement(BoutonDevis, {
      clic: this.clic
    }), this.state.message, /*#__PURE__*/React.createElement(PetitesLignes, {
      ref: this.petitesLignes
    }));
  }

}

function App() {
  return /*#__PURE__*/React.createElement("section", null, /*#__PURE__*/React.createElement("h1", null, "Afrique"), /*#__PURE__*/React.createElement(GrandVoyage, null));
}

let root = document.getElementById('root');
ReactDOM.render( /*#__PURE__*/React.createElement(App, null), root);