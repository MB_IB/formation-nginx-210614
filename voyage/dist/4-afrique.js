"use strict";

function BoutonDevis(props) {
  function clic() {
    if (props.clic !== undefined) props.clic();
  }

  return /*#__PURE__*/React.createElement("button", {
    onClick: clic
  }, "devis");
}

class PetitesLignes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      affiche: true
    };
  }

  masquer() {
    this.setState({
      affiche: false
    });
  }

  render() {
    const style = {
      visibility: this.state.affiche ? "visible" : "hidden"
    };
    return /*#__PURE__*/React.createElement("p", {
      style: style
    }, "Attention, le retour n'est pas inclu dans le tarif.");
  }

}

class ZoneTexte extends React.Component {
  constructor(props) {
    super(props);
    this.sourisDedans = this.sourisDedans.bind(this);
    this.sourisDehors = this.sourisDehors.bind(this);
    this.changer = this.changer.bind(this);
    this.state = {
      "dedans": false,
      value: ""
    };
  }

  sourisDedans() {
    this.setState({
      "dedans": true
    });
  }

  sourisDehors() {
    this.setState({
      "dedans": false
    });
  }

  changer(e) {
    this.setState({
      value: e.target.value
    });
  }

  lire() {
    return this.state.value;
  }

  render() {
    let style = {
      background: "white"
    };
    if (this.state.dedans) style = {
      background: "#FDF"
    };
    return /*#__PURE__*/React.createElement("p", {
      onMouseEnter: this.sourisDedans,
      onMouseLeave: this.sourisDehors
    }, /*#__PURE__*/React.createElement("label", null, this.props.nom), /*#__PURE__*/React.createElement("input", {
      type: "text",
      style: style,
      value: this.state.value,
      onChange: this.changer
    }));
  }

}

class Formulaire extends React.Component {
  constructor(props) {
    super(props);
    this.champs = {};
  }

  lireChamp(s) {
    return this.champs[s].lire();
  }

  render() {
    return /*#__PURE__*/React.createElement("form", null, /*#__PURE__*/React.createElement(ZoneTexte, {
      nom: "Prenom",
      ref: o => {
        this.champs.prenom = o;
      }
    }), /*#__PURE__*/React.createElement(ZoneTexte, {
      nom: "Nom",
      ref: o => {
        this.champs.nom = o;
      }
    }), /*#__PURE__*/React.createElement(ZoneTexte, {
      nom: "Telephone",
      ref: o => {
        this.champs['tel'] = o;
      }
    }), /*#__PURE__*/React.createElement(ZoneTexte, {
      nom: "Email",
      ref: o => {
        this.champs.email = o;
      }
    }));
  }

}

class GrandVoyage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ""
    };
    this.clic = this.clic.bind(this); //this.petitesLignes = null

    this.petitesLignes = React.createRef();
    this.formulaire = React.createRef();
  }

  clic() {
    const prenom = this.formulaire.current.lireChamp("prenom");
    const nom = this.formulaire.current.lireChamp("nom");
    console.log("Demande de devis");
    this.setState({
      message: /*#__PURE__*/React.createElement("p", null, "Demande de ", prenom, " ", nom, " envoy\xE9e")
    }); // this.petitesLignes.masquer()

    this.petitesLignes.current.masquer();
  }

  render() {
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h2", null, "Notre grand voyage"), /*#__PURE__*/React.createElement("p", null, "Parcourez le d\xE9sert et la savane.."), /*#__PURE__*/React.createElement(Formulaire, {
      ref: this.formulaire
    }), /*#__PURE__*/React.createElement(BoutonDevis, {
      clic: this.clic
    }), this.state.message, /*#__PURE__*/React.createElement(PetitesLignes, {
      ref: this.petitesLignes
    }));
  }

}

function App() {
  return /*#__PURE__*/React.createElement("section", null, /*#__PURE__*/React.createElement("h1", null, "Afrique"), /*#__PURE__*/React.createElement(GrandVoyage, null));
}

let root = document.getElementById('root');
ReactDOM.render( /*#__PURE__*/React.createElement(App, null), root);