import { ChampTexte } from './6-japon-champtexte.js'

export function Formulaire() {
	let [actif, setActif] = React.useState(true)
	let [nom, changeNom] = React.useState("")
	let [motDePasse, changeMotDePasse] = React.useState("")
	let [tel, changeTel] = React.useState("")
	let [email, changeEmail] = React.useState("")	
	let [message, setMessage] = React.useState(<p></p>)	
	//setInterval(()=>{ setActif(!actif) }, 1000)
	React.useEffect(
		()=>{ console.log("rendu") }
	)
	let champs = {}
	let clic = () => {
		setActif(false)
		fetch("./login.json?nom="+nom+"&motdepasse="+motDePasse)
			.then(res=>res.json())
			.then(res=>{
				if(res.connection=="ok")
					setMessage( <div>
						<p>Demande pour {nom} envoyée</p> 
						<p>Code {res.codepromo}</p> 
					</div> )
			})
		setMessage( <p>Demande pour {nom} envoyée</p> )
	}
	return <article>
		<ChampTexte nom="Votre nom" fonction={changeNom} />
		<ChampTexte nom="Votre mot de passe" fonction={changeMotDePasse} />
		<ChampTexte nom="Votre telephone" fonction={changeTel} />
		<ChampTexte nom="Votre email" fonction={changeEmail} />
		<button disabled={!actif}
			onClick={clic}>Envoi</button>
		{message}
	</article>
}