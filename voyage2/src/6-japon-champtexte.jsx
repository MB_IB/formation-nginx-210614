export function ChampTexte(props) {
	let [texte, setTexte] = React.useState("")
	let change = (e)=>{
		let v = e.target.value
		setTexte(v)
		props.fonction(v)
	}
	return <p>
		<label>{props.nom}</label>
		<input value={texte} onChange={change} />
	</p>
}