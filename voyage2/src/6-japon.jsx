// attendre React 19 : import { ReactDom } from 'react-dom.js'
import { Formulaire } from './6-japon-formulaire.js'

function App() {
	return <section>
		<h1>Japon</h1>
		<Formulaire />
	</section>
	
}

let root = document.getElementById('root')
ReactDOM.render(<App />, root)