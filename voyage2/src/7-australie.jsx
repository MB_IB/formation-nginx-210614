
import { Description } from './7-australie-description.js'
import { Voyages } from './7-australie-voyages.js'

function App() {
	const villes = ["Adelaïde", "Sydney", "Melbourne"]
	const villesLi = villes.map((v,k) => <li key={k} >{v}</li>)
	// ou : BrowserRouter
	return <>
		<h1>Australie</h1>
		<ul>{villesLi}</ul>		
		<ReactRouterDOM.HashRouter>             
			<p>
				<ReactRouterDOM.Link to="/desc">
					Description
				</ReactRouterDOM.Link>
				|
				<ReactRouterDOM.Link to="/voy">
					Voyages
				</ReactRouterDOM.Link>			
			</p>
			<ReactRouterDOM.Switch>
				<ReactRouterDOM.Route path="/desc">
					<Description />
				</ReactRouterDOM.Route>
				<ReactRouterDOM.Route path="/voy">
					<Voyages />
				</ReactRouterDOM.Route>
			</ReactRouterDOM.Switch>
		</ReactRouterDOM.HashRouter> 
	</>
	
}

let root = document.getElementById('root')
ReactDOM.render(<App />, root)