module.exports = {
	"presets": [
		["@babel/react"],
		["@babel/env", {
			"debug":false,
			"targets":"> 1%",
			/* dans navigateur : */ "modules": false /* enlever pour tests*/
		}]
	]	
}