import React from 'react'
import ReactDOM from 'react-dom'
import { Voyages } from '../src/7-australie-voyages.jsx'

test('Le composant a un titre', ()=>{
	const v = Voyages()
	expect(v.props.children[0].type).toEqual("h1")	
	
})